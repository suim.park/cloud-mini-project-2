use lambda_http::{handler, lambda_runtime::{self, Context, Error}, IntoResponse, Request};
use serde_json::Value;

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler(func)).await?;
    Ok(())
}

async fn func(request: Request, _: Context) -> Result<impl IntoResponse, Error> {
    let body = request.body();
    let body_str = std::str::from_utf8(body).unwrap_or("");
    let json: Value = serde_json::from_str(body_str).unwrap_or_else(|_| serde_json::json!({"message": "Invalid JSON"}));

    let message = json.get("message").and_then(|m| m.as_str()).unwrap_or("");
    let response_message = message.to_uppercase();
    let response_body = serde_json::json!({
        "message": response_message
    });

    Ok(response_body.to_string().into_response())
}
