deploy: build
	cargo lambda deploy lambda-function
watch:
	cargo lambda watch
build:
	cargo lambda build --release
